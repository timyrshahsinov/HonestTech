const localeDomains = require('./config/locale-domains')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Honest Technology',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' },
      { name: 'format-detection', content: 'telephone=no' },
      { property: 'og:title', content: 'Honest Technology' },
      { property: 'og:type', content: 'website' },
      { property: 'og:site_name', content: 'Honest Technology' },
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicons/favicon.png' },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/styles/main.scss',
    'swiper/css/swiper.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/vue-awesome-swiper', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
  ],

  loading: '~/components/s-loader/s-loader',

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  i18n: {
    defaultLocale: 'en',
    locales: [
      {
        code: 'cn',
        iso: 'cn-CN',
        // domain: localeDomains.cn
      },
      {
        code: 'en',
        iso: 'en-US',
        // domain: localeDomains.en
      },
    ],
    differentDomains: false,
    vueI18nLoader: true,
    detectBrowserLanguage: false
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}

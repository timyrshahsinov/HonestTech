import Vue from 'vue';
import { Radar, mixins } from 'vue-chartjs';

Vue.component('line-chart', {
  extends: Radar,
  props: ['chartData', 'options'],
  mixins: [mixins.reactiveProp],
  watch: {
    options: {
      handler() {
        this.renderChart(this.chartData, this.options);
      },
      deep: true,
    },
  },
  mounted() {
    this.renderChart(this.chartData, this.options);
  },
});
